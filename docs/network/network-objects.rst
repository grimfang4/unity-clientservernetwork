Networked Objects
=======================================

Networked objects (as well as RPCs), make up the bulk of what the networking library supports. Most full games will be built using a combination of networked objects, as well as remote procedure calls. Knowing when to use one over the other is entirely up to the developers of the game, but some best practices can guide those decisions.

In general, a networked object is a game object which is instantiated on every client connected to the server. As the object moves on one client, information about it's transform is automatically sent through the server to the other connected clients. A simple example of this may be a game object representing a player's main character. When the game object representing the character is instantiated, it will automatically be created on all other clients connected to the server. As the player moves their character in the simulation, the game object representing that character on all of the other clients will move as well, keeping all of their positions and rotations in sync.


Instantiating
-------------
TODO: How do we instantiate networked game objects over the network? Discuss how the client can spawn objects, as well as how the server can spawn objects. Discuss the requirements for including the prefabs in the Resources directory, as well as explaining why this is a requirement. Talk about the requirement for a Network Sync component. Talk about unique network object ids, and how they are assigned.


Destroying
-------------
TODO: How do we destroy networked game objects? Are there any special considerations we need to make when destroying game objects?


Network Sync Component Details
------------------------------
TODO: What does a network sync component provide? Why is it required? What callbacks does the network sync component provide to the object it is attached to, if any, and what additional functionality does it provide?


Remote Procedure Calls
----------------------
TODO: Discuss sending remote procedure calls to a single game object. Discuss how sending a RPC to a single game object works, and why you would use it. More detail regarding RPCs will be provided in the RPC documentation.


Smooth Movement
---------------
TODO: Discuss network interpolation and extrapolation. Why is this needed? How does it account for lag? What considerations would need to be make regarding objects with physics, or fast paced action games?
