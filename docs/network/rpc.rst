Remote Procedure Calls (RPCs)
=======================================

Overview
--------
Remote Procedure Calls are a simple way to fit networking interactions into a typical game's code structure. They can be conceptualized to work the same way as normal function calls, but initiated by a remote machine. Once communicated across the network, RPCs use Unity's SendMessage() to call the matching method on the receiving object (indicated by the object's network ID).

Format of an RPC
----------------
TODO: Discuss the general way to call RPCs, including what parameters the CallRPC function allows. Discuss how to define RPC functions.


RPCs from the client
--------------------
TODO: Discuss calling RPCs from the client to the server. Examples on why you would do this.


RPCs from the server
--------------------
TODO: Calling RPCs from the server to the clients (or single client). Examples on why you would do this.


Determining where an RPC goes - MessageReceiver
-----------------------------------------------
TODO: Discuss the different options for the MessageReceiver parameter for the CallRPC function. What does each of them mean, and why would you use each? (ServerOnly = 1, AllClients = 2, OtherClients = 4, AllClientsInArea = 8, OtherClientsInArea = 16, SingleClient = 32)


Supported Data Types
--------------------
TODO: Discuss the data types that the networking library allows you to send. Discuss how to add additional data types (WriteRPCParams and ReadRPCParams).


RPCs from one client to another
-------------------------------
TODO: Discuss how and why clients would call RPCs that arrive to a single other client. Discuss why this isn't natually supported by the networking library.
